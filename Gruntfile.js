module.exports = function (grunt) {

  // project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      pre: {
        src: ['data/*.js', 'data/*.css', 'data/*.htm', 'data/*.png']
      }
    },
    eslint: {
      src: ['src/*.js']
    },
    browserify: {
      options: {
        browserifyOptions: {
          debug: true
        }
      },
      dist: {
        files: {
          'data/index.js': 'src/index.js',
          'data/config.js': 'src/config.js'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%=pkg.version %> ' + +new Date() + ' */'
      },
      build: {
        files: {
          'data/index.js': 'data/index.js',
          'data/config.js': 'data/config.js'
        }
      }
    },
    grunt: {
      bootstrap: {
        gruntfile: 'src/bootstrap/Gruntfile.js',
        tasks: ['dist']
      }
    },
    copy: {
      bootstrap: {
        files: [
          {expand: true, cwd: 'src/bootstrap/dist/css/', src: ['bootstrap.min.css'], dest: 'data/'}
        ]
      },
      icons: {
        files: [
          {expand: true, cwd: 'src/icons/', src: ['outlet-60.png'], dest: 'data/'},
          {expand: true, cwd: 'src/icons/', src: ['outlet-76.png'], dest: 'data/'},
          {expand: true, cwd: 'src/icons/', src: ['outlet-120.png'], dest: 'data/'},
          {expand: true, cwd: 'src/icons/', src: ['outlet-152.png'], dest: 'data/'},
          {expand: true, cwd: 'src/icons/', src: ['outlet-192.png'], dest: 'data/'}
        ]
      }
    },
    pug: {
      compile: {
        options: {
          data: {
            debug: false
          }
        },
        files: {
          'data/index.htm': 'src/index.pug',
          'data/config.htm': 'src/config.pug'
        }
      }
    },
    compress: {
      gzip: {
        options: {
          mode: 'gzip'
        },
        files: [
          {
            expand: true, 
            cwd: 'data/', 
            src: ['*.js', '*.css', '*.htm'],
            dest: 'data/'
          }
        ]
      }
    }
  });
  
  // load task plugins
  require('load-grunt-tasks')(grunt, {pattern: ['grunt-*', 'gruntify-*']});

  // default task
  grunt.registerTask('default', 
  [
    'clean:pre',
    'browserify:dist',
    'eslint',
    'uglify', 
    'grunt:bootstrap', 
    'copy:bootstrap',
    'pug:compile',
    'compress:gzip',
    'copy:icons'
  ]);

  // debug task
  grunt.registerTask('debug', 
  [
    'clean:pre',
    'eslint',
    'browserify:dist',
    'grunt:bootstrap', 
    'copy:bootstrap',
    'pug:compile',
    'copy:icons'
  ]);

};