# Switch Bits
Replacement firmware for [Eco Plugs Indoor Wi-Fi Controlled Outlet](https://www.walmart.com/ip/Eco-Plugs-Indoor-Wi-fi-Controlled-Outlet-White/164169458) available from Walmart.

![outlet.jpg](https://bitbucket.org/repo/Kk7pAz/images/2625998787-outlet.jpg)

* WiFi Manager puts the device in AP mode to capture WiFi credentials if none have been set or if the network is unavailable.
* Device can be controlled via HTTP or WebSockets
* Physical button on device toggles outlet state
* Broadcasts changes to WebSockets clients
* Configurable HTTP callback URL
* Configurable auto on mode and device description
* mDNS hostname `switch-ffffff`; where `ffffff` is the last 3 octets of the device's MAC address in lowercase.
* OTA update: POST compiled binary to `/update`
* Blue status LED blinks momentarily while waiting to connect to WiFi. Continued rapid blinking indicates the device has entered AP configuration mode.

## HTTP API
### Get State
_Get the state of the relay and LED._  
**URL** `/state`  
**Method** `GET`  
**Success Response**  
_A JSON string representing the state of the relay and LED. Possible states are "true" and "false"._

  * **Code** 200
  * **Content-Type** application/json  
  * **Content**

```json
{
  "relay": true,
  "led": false
}
```
**Sample Call**  
```sh
curl http://switch-ffffff.local/state
```

### Set State
_Set the state of the relay and LED._  
**URL** `/state`  
**Method** `POST`  
**Content-Type** application/x-www-form-urlencoded  
**Data Params**  
_Data must be submitted as x-www-form-urlencoded. Accepted parameters are relay and led. Possible values are "true", "false", and "toggle". All parameters are optional._
```
relay: [true|false|toggle]
led: [true|false|toggle]
```
**Success Response**  
_A JSON string representing the state of the relay and LED. Possible states are "true" and "false"._

  * **Code** 200
  * **Content-Type** application/json  
  * **Content**

```json
{
  "relay": true,
  "led": false
}
```
**Sample Call**  
```sh
curl -X POST -F 'led=toggle' -F 'relay=toggle' http://switch-ffffff.local/state 
```

### Get Settings
_Get device settings._  
**URL** `/settings`  
**Method** `GET`  
**Success Response**  
_A JSON string representing the state of device settings._

  * **Code** 200
  * **Content-Type** application/json  
  * **Content**

```json
{
  "ssid": "SSID",
  "hostname": "switch-ffffff",
  "ip": "192.168.0.1",
  "description": "Switch Bits",
  "autoOn": false,
  "callback": "http://example.com/callback"
}
```
**Sample Call**  
```sh
curl http://switch-ffffff.local/settings
```

### Set Settings
_Set device settings._  
**URL** `/settings`  
**Method** `POST`  
**Content-Type** application/x-www-form-urlencoded  
**Data Params**  
_Data must be submitted as x-www-form-urlencoded. Accepted parameters are description, autoOn, and callback. All parameters are optional._
```
description: [string]
autoOn: [true|false]
callback: [string]
```
**Success Response**  
_A JSON string representing the state of device settings._

  * **Code** 200
  * **Content-Type** application/json  
  * **Content**

```json
{
  "ssid": "SSID",
  "hostname": "switch-ffffff",
  "ip": "192.168.0.1",
  "description": "Switch Bits",
  "autoOn": false,
  "callback": "http://example.com/callback"
}
```
**Sample Call**  
```sh
curl -X POST -F 'description=Test Lamp' -F 'autoOn=true' -F 'callback=http://example.com/callback' http://switch-ffffff.local/settings 
```
**Notes**
SSID can only be changed be resetting the device and selecting a new network in the captive portal Wifi Manager. SSID, hostname, and ip are read-only settings. If set, a JSON string (Content-Type: application/json) containing both state and status is posted to the callback URL. The callback is removed by setting it to an empty string. Description defaults to "Switch Bits" and can be blank. 

**Sample callback body:**
```json
{ 
  "ssid": "SSID",
  "hostname": "switch-ffffff",
  "ip": "192.168.0.1",
  "description": "Switch Bits",
  "autoOn": false,
  "callback": "http://example.com/callback",
  "relay": true,
  "led": false 
}
```

### Get Relay
_Get relay state._  
**URL** `/relay`  
**Method** `GET`  
**Success Response**  
_The plain text string "true" or "false"._

  * **Code** 200
  * **Content-Type** text/plain  
  * **Content**

```
false
```
**Sample Call**  
```sh
curl http://switch-ffffff.local/relay
```

### Set Relay
_Set relay state._  
**URL** `/relay`  
**Method** `POST`  
**Content-Type** application/x-www-form-urlencoded  
**Data Params**  
_Data must be submitted as x-www-form-urlencoded. Accepted parameter is "relay". Possible values: "true", "false", or "toggle". All parameters are optional._
```
relay: [true|false|toggle]
```
**Success Response**  
_A plain text string representing the state of the relay._

  * **Code** 200
  * **Content-Type** text/plain  
  * **Content**

```
true
```
**Sample Call**  
```sh
curl -X POST -F 'relay=toggle' http://switch-ffffff.local/relay 
```

### Get LED
_Get LED state._  
**URL** `/led`  
**Method** `GET`  
**Success Response**  
_The plain text string "true" or "false"._

  * **Code** 200
  * **Content-Type** text/plain  
  * **Content**

```
false
```
**Sample Call**  
```sh
curl http://switch-ffffff.local/led
```

### Set LED
_Set LED state._  
**URL** `/led`  
**Method** `POST`  
**Content-Type** application/x-www-form-urlencoded  
**Data Params**  
_Data must be submitted as x-www-form-urlencoded. Accepted parameter is "led". Possible values: "true", "false", or "toggle". All parameters are optional._
```
led: [true|false|toggle]
```
**Success Response**  
_A plain text string representing the state of the LED._

  * **Code** 200
  * **Content-Type** text/plain  
  * **Content**

```
true
```
**Sample Call**  
```sh
curl -X POST -F 'led=toggle' http://switch-ffffff.local/led 
```

### Restart
_Restart the device._  
**URL** `/restart`  
**Method** `GET`  
**Success Response**  
_Empty response._

  * **Code** 200
  * **Content-Type** text/plain  

**Sample Call**  
```sh
curl http://switch-ffffff.local/restart
```

### Reset
_Reset the device. Removes WiFi credentials. Sets description and autoOn to defaults.  
**URL** `/reset`  
**Method** `GET`  
**Success Response**  
_Empty response._

  * **Code** 200
  * **Content-Type** text/plain  

**Sample Call**  
```sh
curl http://switch-ffffff.local/reset
```
***Notes***
This does not reset the callback URL. To remove the callback, set it to the empty string.


## WebSockets API
The WebSockets server is listening on port 81. State, description, and autoOn change events, as well as restart and reset notices are broadcast to all connected clients.

### Commands
_Once connected, send commands with `connection.send()`._  


`t` toggle relay  
`l` toggle LED  
`s` send state to requesting client  
`b` broadcast state to all clients  
`0` relay off  
`1` relay on  
`2` LED off  
`3` LED on  
`r` restart  
`w` reset

### Messages
_Once connected, clients may recieve messages from the device with `connection.onmessage = fn`._  


`0|0` relay off, led off  
`1|0` relay on, led off  
`0|1` relay off, led on  
`1|1` relay on, led on  
`restart` the device will now restart  
`reset` the device will now reset  

### JavaScript Example
```js
var connection = new WebSocket("ws://switch-ffffff.local:81/", 'ardunio');

connection.onconnecting = function (event) {
  console.log('connecting');
};

connection.onopen = function (event) {
  console.log('connected');
};

connection.onclose = function (event) {
  console.log('disconnected');
};

connection.onmessage = function (event) {
  console.log('message: ', event.data);
  
  var relayState = event.data.split('|')[0]
    , ledState   = event.data.split('|')[1];
};

connection.onerror = function (event) {
  console.log('an error occurred');
};
```

## Modifying the UI

### Setting up your environment
1. Install the LTS version of `node` from [nodejs.org](https://nodejs.org), which provides the `npm` package manager.

1. Install `grunt-cli` using `npm` from the command line:

        #!sh
        sudo npm install -g grunt-cli

1. Install Switch Bits' NPM dependencies:

        #!sh
        cd switchbits
        npm install

1. Install Bootstrap's NPM dependencies:

        #!sh
        cd switchbits/src/bootstrap
        npm install

### Modifying UI source files

TODO document modifying variables.less, bootstrap.less, switchbits.less, index.html, index.js

### Rebuilding minified files and the SD zip file ###

1. Issue the `grunt` command to rebuild UI files.

        #!sh
        cd switchbits
        grunt