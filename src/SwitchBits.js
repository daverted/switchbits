/* eslint no-unused-vars: "off" */
/* global module */

var util = require('./util');

// export class SwitchBits
module.exports = SwitchBits;

// constructor
function SwitchBits () {
  // reference this
  var self = this;

  // set state to connecting
  this.updateWsState('connecting');
  
  // connect to switch via websockets on port 81
  this.connection = new WebSocket('ws://' + location.hostname + ':81/', 'arduino');
  
  // socket connecting event handler
  this.connection.onconnecting = function (event) {
    // here, "this" is the WebSocket object
    self.updateWsState('connecting');
  };
  
  // socket open event handler
  this.connection.onopen = function (event) {
    self.updateWsState('connected');
  };
  
  // socket close event handler
  this.connection.onclose = function (event) {
    self.updateWsState('disconnected');
    
    // TODO show reconnect button - for now, refresh
    location.reload();
  };
  
  // socket error event handler
  this.connection.onerror = function (event) {
    // TODO handle errors
  };

  // attach click listener to led
  this.plug.addEventListener('click', function() {
    // toggle led state
    self.connection.send('l');
  });
}

SwitchBits.prototype.wsDisconnected = util.$1('#wsDisconnected');
SwitchBits.prototype.wsConnecting   = util.$1('#wsConnecting');
SwitchBits.prototype.wsConnected    = util.$1('#wsConnected');

SwitchBits.prototype.plug           = util.$1('#plug');
SwitchBits.prototype.description    = util.$1('#description');

SwitchBits.prototype.ledState;
SwitchBits.prototype.connection;

var on         = '1'
  , hidden     = 'none'
  , visible    = 'inline-block'
  , ledOn      = 'a100'
  , ledOff     = 'p100';

// draw plug icon (physical LED) state
SwitchBits.prototype.redrawLedState = function() {
  if (this.ledState === true) {
    this.plug.classList.remove(ledOff);
    this.plug.classList.add(ledOn);
  } else {
    this.plug.classList.remove(ledOn);
    this.plug.classList.add(ledOff);
  }
};

// set led state
SwitchBits.prototype.setLed = function (state) {
  // update local variable
  this.ledState = (state === on);

  // update UI
  this.redrawLedState();
};

// update websocket state
SwitchBits.prototype.updateWsState = function (state) {
  // hide all
  this.wsDisconnected.style.display = hidden;
  this.wsConnecting.style.display = hidden;
  this.wsConnected.style.display = hidden;

  // show correct state
  switch (state) {
  case 'disconnected':
    this.wsDisconnected.style.display = visible;
    break;
  case 'connecting':
    this.wsConnecting.style.display = visible;
    break;
  case 'connected':
    this.wsConnected.style.display = visible;
    break;
  }  
};
