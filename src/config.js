/* eslint no-unused-vars: "off", no-console: "off" */

var util       = require('./util')
  , SwitchBits = require('./SwitchBits');

(function(){

  var sb = new SwitchBits();
  
  // socket message event handler
  sb.connection.onmessage = function (event) {
    // parse response
    var ledState = event.data.split('|')[1];

    // update states
    sb.setLed(ledState);
  };

  // show settings in UI
  util.get('/settings', function(settings) {
    // avoid errors
    if (typeof settings !== 'object') return;

    // set description
    // sb.description.innerText = settings.description;

    /*
    // set ip
    var a = document.createElement('A');
    a.href = 'http://' + settings.ip + '/';
    a.innerText = settings.ip;
    ip.appendChild(a);

    // set hostname
    a = document.createElement('A');
    a.href = 'http://' + settings.hostname + '.local/';
    a.innerText = settings.hostname;
    hostname.appendChild(a);
    */

    // set autoOn
    // TODO settings.autoOn === "false";
  });

})();