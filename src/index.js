/* eslint no-unused-vars: "off", no-console: "off" */

var util       = require('./util')
  , SwitchBits = require('./SwitchBits');

var on = '1';

SwitchBits.prototype.button = util.$1('#button');
SwitchBits.prototype.relayState = util.$1('#state');

SwitchBits.prototype.redrawRelayState = function() {
  // button.checked
  if (this.button.checked) {
    // on
    this.relayState.innerHTML = '<i class="state-led on"></i>On';
  } else {
    // off
    this.relayState.innerHTML = '<i class="state-led"></i>Off';
  }
};

// set relay state
SwitchBits.prototype.setRelay = function (state) {
  // enable button
  this.button.disabled = false;
  
  // set on/off state
  this.button.checked = (state === on);
  
  // update UI
  this.redrawRelayState();
};

(function(){

  var sb = new SwitchBits();

  // socket message event handler
  sb.connection.onmessage = function (event) {
    console.info('MESSAGE: ', event.data);
    
    // parse response
    var relayState = event.data.split('|')[0]
      , ledState   = event.data.split('|')[1];

    // update states
    sb.setRelay(relayState);
    sb.setLed(ledState);
  };

  // attach change listener
  sb.button.addEventListener('change', function() {
    // disable button
    sb.button.disabled = true;
    
    // toggle relay state
    sb.connection.send('t');
  });

  // show settings in UI
  util.get('/settings', function(settings) {
    // avoid errors
    if (typeof settings !== 'object') return;

    // set description
    sb.description.innerText = settings.description;
    // set page title
    document.title = settings.description;
  });

})();