/* global module */

module.exports = {
  // select a list of matching elements, context is optional
  $: function (selector, context) {
    return (context || document).querySelectorAll(selector);
  },
  // select the first match only, context is optional
  $1: function (selector, context) {
    return (context || document).querySelector(selector);
  },
  // make ajax calls
  request: function (method, path, payload, callback) {
    var xhttp = new XMLHttpRequest();

    // avoid back/forward caching problems
    path += '?' + +new Date();

    xhttp.open(method, path, true);
    xhttp.send(payload);
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        // xhttp.responseURL = http://location.hostname/path
        callback(JSON.parse(xhttp.response));
      }
    };
  },
  get: function (path, callback) {
    this.request('GET', path, null, callback);
  },
  post: function (path, payload, callback) {
    this.request('POST', path, payload, callback);
  }
};
