/* https://bitbucket.org/daverted/switchbits/
 *
 * Eco Plugs Indoor Wi-Fi Controlled Outlet from Walmart
 * ----------------------------------------------------------------------------------
 * ESP Pinout:
 * ----------------------------------------------------------------------------------
 * 
 *            -------------------
 *          --|             GND |-- 
 *          --| VCC             |-- 
 *          --| Reset           |--
 *          --|              TX |-- Serial TX/Prog RX
 *          --|              RX |-- Serial RX/Prog TX
 *   Button --| GPIO13    GPIO5 |--
 *    Relay --| GPIO15    GPIO4 |-- 
 * Blue LED --| GPIO2     GPIO0 |-- Bootloader (low - program, high - normal)
 *            -------------------
 * ----------------------------------------------------------------------------------
   Board: Generic ESP8266 Module
   CPU Frequency: 80 Mhz
   Flash Size: 1M (128K SPIFFS)
   TODO Web UI files in SPIFFS
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h> // https://bitbucket.org/akudaikon/esp8266httpupdateserverspiffs.git
#include <ESP8266HTTPClient.h>
#include <Hash.h>
#include <Button.h> // https://github.com/JChristensen/Button
#include <DNSServer.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <EEPROM.h>
#include <FS.h>

#define LED_PIN 2 // 5: thingdev 2: ecoplug
#define RELAY_PIN 15
#define BUTTONPIN 13
#define OFF false
#define ON true

// global vars
char host[13];
bool autoOn = false;
String description;
String callback;

enum EEPROMSettings {
  SETTING_INITIALIZED,
  SETTING_AUTOON,
  NUM_OF_SETTINGS
};

WiFiManager wifiManager; //https://github.com/tzapu/WiFiManager
WebSocketsServer webSocket = WebSocketsServer(81);
ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;
Ticker ticker;

// pin, pullup, invert, debounce_ms
Button btn(BUTTONPIN, true, true, 100);

void relayOn() {
  digitalWrite(RELAY_PIN, ON);
  Serial.println("Relay ON");
  broadcastState();
  cb();
}

void relayOff() {
  digitalWrite(RELAY_PIN, OFF);
  Serial.println("Relay OFF");
  broadcastState();
  cb();
}

bool relayState() {
  return digitalRead(RELAY_PIN);
}

void toggleRelay() {
  relayState() ? relayOff() : relayOn();
}

void ledOn() {
  digitalWrite(LED_PIN, !ON); // active low
  Serial.println("LED ON");
  broadcastState();
  cb();
}

void ledOff() {
  digitalWrite(LED_PIN, !OFF);
  Serial.println("LED OFF");
  broadcastState();
  cb();
}

bool ledState() {
  return !digitalRead(LED_PIN); // active low
}

void toggleLed() {
  ledState() ? ledOff() : ledOn();
}

/* broadcast a string representing both relay and led states
   0|0    relay off, led off
   1|0    relay on,  led off
   0|1    relay off, led on
   1|1    relay on,  led on */ 
void broadcastState() {
  char state[3];
  sprintf(state, "%u|%u", relayState(), ledState());
  webSocket.broadcastTXT(state);
}

void sendState(uint8_t num) {
  char state[3];
  sprintf(state, "%u|%u", relayState(), ledState());
  webSocket.sendTXT(num, state);
}

// restart the device
void restart() {
  webSocket.broadcastTXT("restart");
  ESP.restart();
}

// reset wifi credentials, description, and auto on
void reset() {
  webSocket.broadcastTXT("reset");
  wifiManager.resetSettings();
  if (SPIFFS.exists("description")) SPIFFS.remove("description");
  initSettings();
  ESP.restart();
}

// init EEPROM settings
void initSettings() {
  EEPROM.write(SETTING_INITIALIZED,  0x55); // EEPROM initalized
  EEPROM.write(SETTING_AUTOON,       0);    // Relay on at powerup
}

// read EEPROM settings
void readSettings() {
  if (EEPROM.read(SETTING_INITIALIZED) != 0x55) initSettings();
  autoOn = EEPROM.read(SETTING_AUTOON);
}

// read SPIFFS
void readSpiffs() {
  if (SPIFFS.exists("description")) {
    File descriptionFile = SPIFFS.open("description", "r");
    description = descriptionFile.readStringUntil('\n');
    description = description.substring(0, description.length() - 1); // remove trailing newline
    descriptionFile.close();
  } else {
    description = "Switch Bits";
  }
  
  if (SPIFFS.exists("callback")) {
    File callbackFile = SPIFFS.open("callback", "r");
    callback = callbackFile.readStringUntil('\n');
    callback = callback.substring(0, callback.length() - 1); // remove trailing newline
    callbackFile.close();
  } else {
    callback = "";
  }
}

// callback to notify remote server
void cb() {
  if (callback == "") return;

  String response = "";

  response += "{";
  response += settingsToJSON(false);
  response += ",";
  response += stateToJSON(false);
  response += "}";

  Serial.println(response);
  
  HTTPClient http;
  http.begin(callback);
  http.addHeader("Content-Type", "application/json");
  http.POST(response);
  http.end();
}

String settingsToJSON(bool close) {
  String response = "";
  IPAddress ip = WiFi.localIP();

  if (close) response += "{";

  response += "\"ssid\":";
  response += "\"" + WiFi.SSID() + "\"";
  response += ",\"hostname\":\"";
  response += host;
  response += "\",\"ip\":\"";
  response += String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);
  response += "\",\"description\":\"";
  response += description;
  response += "\",\"autoOn\":\"";
  response += (autoOn ? "true" : "false");

  if (callback != "") {
    response += "\",\"callback\":\"";
    response += callback;    
  }

  if (close) {
    response += "\"}";
  } else {
    response += "\"";
  }
  
  return response;
}

void getSettings() {
  // don't cache this response
  httpServer.sendHeader("Cache-Control", "max-age=0");
  httpServer.send(200, "application/json", settingsToJSON(true));
}

void postSettings() {

  // set outlet description
  if (httpServer.hasArg("description")) {
    description = httpServer.arg("description");
    // remove existing description
    if (SPIFFS.exists("description")) SPIFFS.remove("description");
    // set new description
    File descriptionFile = SPIFFS.open("description", "w");
    descriptionFile.println(description);
    descriptionFile.close();
    // broadcast change to ws clients
    webSocket.broadcastTXT("description|" + description);
  }

  // auto on
  if (httpServer.hasArg("autoOn")) {
    autoOn = (httpServer.arg("autoOn") == "true" ? true : false);
    EEPROM.write(SETTING_AUTOON, autoOn);
    EEPROM.commit();
    // broadcast change to ws clients
    // TODO only trigger when setting has changed
    webSocket.broadcastTXT("autoOn|" + httpServer.arg("autoOn"));
  }

  // set outlet description
  if (httpServer.hasArg("callback")) {
    callback = httpServer.arg("callback");
    // remove existing callback
    if (SPIFFS.exists("callback")) SPIFFS.remove("callback");
    // set new description
    File callbackFile = SPIFFS.open("callback", "w");
    callbackFile.println(callback);
    callbackFile.close();
  }

  // respond with settings
  getSettings();

  // http callback
  // TODO only trigger when a setting has changed
  cb();
}

String stateToJSON(bool close) {
  String response = "";

  if (close) response += "{";

  response += "\"relay\":";
  response += (relayState() ? "true" : "false");
  response += ",\"led\":";
  response += (ledState() ? "true" : "false");
  
  if (close) response += "}";
  
  return response;
}

void getState() {
  // don't cache this response
  httpServer.sendHeader("Cache-Control", "max-age=0");
  httpServer.send(200, "application/json", stateToJSON(true));
}

// sets everyting
void setState() {
  // update relay
  postRelay();
  // update led
  postLed();

  // respond with state
  getState();
}

// handle GET relay
void getRelay() {
  // send plain text boolean representation of relay state
  httpServer.send(200, "text/plain", (relayState() ? "true" : "false"));  
}

// handle POST /relay
void postRelay() {
  // x-www-form-urlencoded key:relay value: true/false/toggle
  if (httpServer.arg("relay") == "true") return relayOn();
  if (httpServer.arg("relay") == "false") return relayOff();
  if (httpServer.arg("relay") == "toggle") return toggleRelay();
}

// handle GET /led
void getLed() {
  // send plain text boolean representation of led state
  httpServer.send(200, "text/plain", (ledState() ? "true" : "false"));
}

// handle POST /led
void postLed() {
  // x-www-form-urlencoded key:led value: true/false/toggle
  if (httpServer.arg("led") == "true") return ledOn();
  if (httpServer.arg("led") == "false") return ledOff();
  if (httpServer.arg("led") == "toggle") return toggleLed();
}

// handle GET /
void getIndex() {
  httpServer.send(200, "text/plain", "see: https://bitbucket.org/daverted/switchbits/");
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {

  switch(type) {
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED: {
      IPAddress ip = webSocket.remoteIP(num);
      Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
    }
      // send state to new client
      sendState(num);
      break;
    case WStype_TEXT:
      Serial.printf("[%u]: %s\n", num, payload);

      // catch connection.send('');
      if (payload == NULL) break;

      if (payload[0] == 't') return toggleRelay();    // t => toggle relay
      if (payload[0] == 'l') return toggleLed();      // l => toggle led
      if (payload[0] == 's') return sendState(num);   // s => send state to this client
      if (payload[0] == 'b') return broadcastState(); // b => broadcast state
      if (payload[0] == '0') return relayOff();       // 0 => turn relay off
      if (payload[0] == '1') return relayOn();        // 1 => turn relay on
      if (payload[0] == '2') return ledOff();         // 2 => turn led off
      if (payload[0] == '3') return ledOn();          // 3 => turn led on
      if (payload[0] == 'r') return restart();        // r => restart device
      if (payload[0] == 'w') return reset();          // w => reset wifi creds
  }

}

// blink the led
void blink() {
  // toggle LED state
  digitalWrite(LED_PIN, !digitalRead(LED_PIN));
}

// gets called when WiFiManager enters config mode
void configModeCallback(WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  Serial.println(myWiFiManager->getConfigPortalSSID());
  // entered config mode; LED blink faster
  ticker.attach_ms(200, blink);
}

// serve up files from SPIFFS
bool handleFileRead(String path) {

  // identify server
  httpServer.sendHeader("X-Powered-By", "RainbowsAndUnicorns");

  // normal use: cache for 1 day
  // httpServer.sendHeader("Cache-Control", "max-age=86400");

  // debugging: disable cache
  httpServer.sendHeader("Cache-Control", "max-age=0");

  // handle root
  if (path.endsWith("/")) path += "index.htm";

  // .htm in the url looks meh
  if (path.endsWith("/config")) path += ".htm";

  if (SPIFFS.exists(path)) {
    // set data type and content encoding
    String dataType = "text/plain";
    if (path.endsWith(".htm")) {
      dataType = "text/html; charset=UTF-8";
      httpServer.sendHeader("Content-Encoding", "gzip"); // gzipped by grunt
    } else if (path.endsWith(".css")) {
      dataType = "text/css; charset=UTF-8";
      httpServer.sendHeader("Content-Encoding", "gzip"); // gzipped by grunt
    } else if (path.endsWith(".js")) {
      dataType = "text/javascript; charset=UTF-8";
      httpServer.sendHeader("Content-Encoding", "gzip"); // gzipped by grunt
    } else if (path.endsWith(".png")) {
      dataType = "image/png";
    }

    File file = SPIFFS.open(path, "r");
    httpServer.streamFile(file, dataType);
    file.close();
    return true;
  }

  return false;
}

void setup() {
  // setup pins
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  // set initial states
  digitalWrite(RELAY_PIN, OFF);
  digitalWrite(LED_PIN, !OFF);

  Serial.begin(115200);

  // initialize EEPROM
  EEPROM.begin(NUM_OF_SETTINGS);
  readSettings();

  // auto on
  if (autoOn) digitalWrite(RELAY_PIN, ON);

  // Initialize the SPIFFS
  SPIFFS.begin();
  readSpiffs();

  // start blinking LED
  ticker.attach_ms(600, blink);
  
  // get mac address
  uint8_t mac[6];
  WiFi.macAddress(mac);

  // make hostname switch-xxxxxx
  sprintf(host, "%s%02x%02x%02x", "switch-", mac[3], mac[4], mac[5]);

  // set config mode callback
  wifiManager.setAPCallback(configModeCallback);

  // fetch ssid and pass and try to connect
  // or go into blocking AP captive portal mode
  if (!wifiManager.autoConnect(host, "configure")) {
    Serial.println("failed to connect and hit timeout");
    // reset and try again
    ESP.reset();
  }

  // if you're here; you've connected to the wifi!
  
  // start mDNS
  if (MDNS.begin(host)) {
    Serial.print("Hostname: ");
    Serial.println(host);
  }

  // start webSocket server
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);

  // HTTP API
  httpServer.on("/state", HTTP_GET, getState);
  httpServer.on("/state", HTTP_POST, setState);
  httpServer.on("/settings", HTTP_GET, getSettings);
  httpServer.on("/settings", HTTP_POST, postSettings);
  httpServer.on("/relay", HTTP_GET, getRelay);
  httpServer.on("/relay", HTTP_POST, [](){ postRelay(); getRelay(); });
  httpServer.on("/led", HTTP_GET, getLed);
  httpServer.on("/led", HTTP_POST, [](){ postLed(); getLed(); });
  httpServer.on("/restart", HTTP_GET, [](){ httpServer.send(200); restart(); });
  httpServer.on("/reset", HTTP_GET, [](){ httpServer.send(200); reset(); });

  // handle web UI
  httpServer.onNotFound([](){
    if (!handleFileRead(httpServer.uri())) {
      httpServer.send(404, "text/plain", "Not Found");
    }
  });

  // update: POST binary to /update
  httpUpdater.setup(&httpServer);

  // start http server
  httpServer.begin();

  // stop blinking LED and turn it off
  ticker.detach();
  ledOff();
}

void loop() {
  // handle WebSockets requests
  webSocket.loop();
  
  // just in case
  yield();
  
  // read button
  btn.read();

  // check for button press
  if (btn.wasPressed()) {
    toggleRelay();
  }
  
  // just in case
  yield();

  httpServer.handleClient();
}
